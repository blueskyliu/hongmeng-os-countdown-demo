package com.example.customtime;

import com.example.customtime.slice.SecondryAbility2Slice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SecondryAbility2 extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SecondryAbility2Slice.class.getName());
    }
}
