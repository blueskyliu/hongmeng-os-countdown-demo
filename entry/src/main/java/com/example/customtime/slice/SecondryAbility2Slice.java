package com.example.customtime.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DependentLayout.LayoutConfig;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;

import java.util.Timer;
import java.util.TimerTask;

public class SecondryAbility2Slice extends AbilitySlice {

    private DependentLayout myLayout = new DependentLayout(this);
    private Timer timer;//定时器
    private int count=0;//记数
    private boolean flag= true;//true 暂停 false 继续
    Button button = new Button(this);
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        LayoutConfig config = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_PARENT);
        myLayout.setLayoutConfig(config);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(new RgbColor(255, 255, 255));
        myLayout.setBackground(element);

        button.setText("hello");
        button.setTextSize(75);
        button.setTextColor(Color.BLACK);
        button.setLayoutConfig(config);
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();//终止当前模块 类似返回功能
            }
        });
        Button btnStop = new Button(this);
        LayoutConfig btnStopconfig = new LayoutConfig(LayoutConfig.MATCH_PARENT, LayoutConfig.MATCH_CONTENT);
        btnStop.setText("暂停");
        btnStop.setTextSize(75);
        btnStop.setTextColor(Color.WHITE);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(0, 125, 255));
        btnStopconfig.addRule(LayoutConfig.ALIGN_PARENT_BOTTOM);
        btnStop.setBackground(shapeElement);
//        btnStopconfig.alignment = LayoutAlignment.VERTICAL_CENTER

        btnStop.setLayoutConfig(btnStopconfig);
        btnStop.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (flag==true){
                    getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            timer.cancel();//取消即使
                            timer=null;
                            button.setText("暂停");
                            btnStop.setText("继续");

                        }
                    });

                }else {
                    getUITaskDispatcher().asyncDispatch(new Runnable() {
                        @Override
                        public void run() {
                            btnStop.setText("暂停");
                            createTimer();
                        }
                    });
                }
                flag = !flag;


            }
        });
        myLayout.addComponent(button);
        myLayout.addComponent(btnStop);

        super.setUIContent(myLayout);
        createTimer();
    }
    private void createTimer(){
        timer = new Timer();
        /**
         *定时器
         * TimerTask
         *  参数1 执行方法
         *  参数2 延迟时间 单位毫秒
         *  参数3 间隔时间 毫秒单位
         */
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                count++;
                /**
                 * 一部分发
                 */
                getUITaskDispatcher().asyncDispatch(new Runnable() {
                    @Override
                    public void run() {
                        button.setText(""+count);
                    }
                });
            }
        },0,1000);
    }
    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

}
